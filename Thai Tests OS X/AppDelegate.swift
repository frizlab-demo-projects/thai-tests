/*
 * AppDelegate.swift
 * Thai Tests OS X
 *
 * Created by François Lamboley on 10/23/15.
 * Copyright © 2015 happn. All rights reserved.
 */

import Cocoa



@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

	func applicationDidFinishLaunching(aNotification: NSNotification) {
		// Insert code here to initialize your application
	}

	func applicationWillTerminate(aNotification: NSNotification) {
		// Insert code here to tear down your application
	}

}
