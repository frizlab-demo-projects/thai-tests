/*
 * ViewController.swift
 * Thai Tests OS X
 *
 * Created by François Lamboley on 10/23/15.
 * Copyright © 2015 happn. All rights reserved.
 */

import Cocoa



class ViewController: NSViewController {
	
	@IBOutlet var labelAttr1: NSTextField!
	@IBOutlet var labelAttr2: NSTextField!
	
	override var representedObject: Any? {
		didSet {
			// Update the view, if already loaded.
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		let ps = NSMutableParagraphStyle()
		ps.lineHeightMultiple = 1.3
		labelAttr1.attributedStringValue = NSAttributedString(string: labelAttr1.stringValue, attributes: [NSAttributedStringKey.paragraphStyle: ps])
		labelAttr2.attributedStringValue = NSAttributedString(string: labelAttr2.stringValue, attributes: [NSAttributedStringKey.paragraphStyle: ps])
	}
	
	@IBAction func copyText(sender: AnyObject!) {
		NSPasteboard.general.clearContents()
		NSPasteboard.general.writeObjects(["               ฝึกปฏิบัติในการใช้งาน\nสวนกับเธอคนนี้ 128 ครั้ง" as NSString])
	}
	
}
