/*
 * ViewController.swift
 * Thai Tests
 *
 * Created by François Lamboley on 10/23/15.
 * Copyright © 2015 happn. All rights reserved.
 */

import UIKit



class ViewController: UIViewController {
	
	@IBOutlet var labelAttr1: UILabel!
	@IBOutlet var labelAttr2: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		let ps = NSMutableParagraphStyle()
		ps.lineHeightMultiple = 1.3
		labelAttr1.attributedText = NSAttributedString(string: labelAttr1.text!, attributes: [NSAttributedStringKey.paragraphStyle: ps])
		labelAttr2.attributedText = NSAttributedString(string: labelAttr2.text!, attributes: [NSAttributedStringKey.paragraphStyle: ps])
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	@IBAction func copyText(sender: AnyObject!) {
		UIPasteboard.general.string = "               ฝึกปฏิบัติในการใช้งาน\nสวนกับเธอคนนี้ 128 ครั้ง"
	}

}
